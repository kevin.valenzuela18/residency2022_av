# Datasets

Data sets related to the adquisition of electroencephalic signals.

| Dataset | File Name | Download Page | Download Link | Date Downloaded |
| :---    | :---      | :---          | :--           | :--             |
| Two class motor imagery (002-2014) | S01E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S01E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S02E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S02E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S03E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S03E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S04E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S04E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S05E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S05E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S06E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S06E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S07E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S07E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S08E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S08E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S09E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S09E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S10E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S10E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S11E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S11E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S12E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S12E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S13E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S13E.mat | 2022-03-05 |
| Two class motor imagery (002-2014) | S14E.mat | http://bnci-horizon-2020.eu/database/data-sets | http://bnci-horizon-2020.eu/database/data-sets/002-2014/S14E.mat | 2022-03-05 |

